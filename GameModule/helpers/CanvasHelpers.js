import {fabric} from 'fabric';


function Annotation(obj, x, y, width, height, vpW, vpH) {
	this.obj = obj;
	this.xCoord = x;
	this.yCoord = y;
	this.widthVal = width;
	this.heightVal = height;
	this.viewportWidth = vpW;
	this.viewportHeight = vpH;
}  

export const Helpers = {
	resizeCanvas(canvas, parent = window, autoHeight = false) {

		let annotationList = [];
		let vpW = parent.offsetWidth;
		let vpH = parent.offsetHeight;
		let adjustedHeight =  Helpers.getCanvasDefaultHeight(vpW)	;
		
		vpH = autoHeight ?  adjustedHeight  : vpH;
		canvas.setHeight(vpH);
		canvas.setWidth(vpW);
		canvas.width = vpW;
		canvas.height = vpH;

		canvas.forEachObject(function(obj){
			if (objInArray(obj, annotationList) <0){
				var x = obj.get('left'),
				y = obj.get('top'),
				width = obj.getWidth(),
				height = obj.getHeight();
				var newAnnotation = new Annotation(obj, x, y, width, height, vpW, vpH);
				annotationList.push(newAnnotation);
			}

			var annotation = annotationList[objInArray(obj, annotationList)];

			var scaleW = (vpW/annotation.viewportWidth);
			var scaleH = (vpH/annotation.viewportHeight);
			var scaleX = annotation.xCoord * scaleW;
			var scaleY = annotation.yCoord * scaleW; 

			obj.set('scaleX', scaleW);
			obj.set('scaleY', scaleW);
			obj.set('left', scaleX);
			obj.set('top', scaleY); 
			obj.setCoords();
		}); 

		canvas.renderAll();
	},
	getCanvasDefaultHeight(contextWidth){
		return contextWidth * 900 / 780;
	},
	createCanvas() {
		let canvas = document.createElement('canvas');
		let canvas = new fabric.Canvas(canvas, {
			isDrawingMode: false,
		});
		Helpers.resizeCanvas(canvas);
		return canvas;
	},
	objInArray(obj, annotationArray) {
		for (let i = 0; i < annotationArray.length; i++) {
			if (annotationArray[i].obj === obj){
				return i;
			}
		} 
		return -1;
	}


}
