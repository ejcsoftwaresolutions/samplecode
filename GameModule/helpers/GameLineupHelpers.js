import  FormationFieldHelpers from './FormationFieldHelpers'
import {getFieldAreaContainerBaseParameters} from "./FormationFieldHelpers"

export const Helpers = {
    findPositionSpotInField(spotId, positionsByArea) {
        let positions =  positionsByArea.map((e, i) => e.positions).reduce((p1, p2) => [...p1, ...p2] )
            let sourceSpot = positions.filter((e) => {
                return e.viewId == spotId
            });
            return sourceSpot;
    },
    fixPositions(positions, fix = 120/4) {
        return positions.map((e,i) => {
            e.coordinates.x= e.coordinates.x - (fix);
            return e;
        })
    },
    getPositionsByArea(positions) {
        let newPositions =  positions 
        let positionsByArea = _.values(FormationFieldHelpers.getPositionsByArea(getFieldAreaContainerBaseParameters().fieldAreasConfigurations.areas, newPositions)) ;
        return positionsByArea;
    },
    getPlayerOutOfBench(bench, playerId) {
        return _.remove(bench, function(e) {
            return e == playerId;
        });
    },
    returnPlayerToBench(bench, playerId) {
        return bench.push(playerId);
    },
    insertPlayerToBench(bench, playerId, position) {
        bench[position] = playerId
        return bench;
    },
    mapSpots(formationPositions, players = null) {
        let playerId;
        players = players || [];

        return  formationPositions.map((e) => {

            let spot = players.filter((p, i) => { return p.spot_view_id == e.view_id })[0]
            playerId = spot ? (_.isObject(spot.player) ? spot.player.id : spot.player )  : null;

            return {
                playerId: playerId,
                viewId: e.view_id,
                style: Helpers.calcSpotPosition(e.x, e.y, e.area), 
                area: e.area
            }
        })
    },
    calcSpotPosition(x, y, area) {

        let spotAreaBaseWidth = 800;
        let areaBaseParameters =  getFieldAreaContainerBaseParameters();
        let spotAreaBaseHeight = area !== "goalkeeper" ? areaBaseParameters.baseAreaHeight :  areaBaseParameters.golkeeperAreaHeight;
        let spotTop = area !== "goalkeeper" ?  ( (100 * y ) / spotAreaBaseHeight ) + 20 + "%" : 0

        return {
            left: x - (8/2) + "%" ,
            top : y + "%" ,
            position: "absolute",
            width: "14%",
            height: area !== "goalkeeper" ? "56%" : "44%"
        }

    },
    fixBenchPlayersPositions() {
        let fixing = $(".player-on-bench").filter(function(i,e){
            return parseInt($(e).css("left")) > 0;
        })
        fixing.each(function(e, i) {
            $(i).removeAttr("style");
            $(i).css({
                position: "relative"
            })
        })
    }
 
}