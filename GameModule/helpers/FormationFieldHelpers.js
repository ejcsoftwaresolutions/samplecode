import  {Helpers as CanvasHelpers} from './CanvasHelpers'
export const FormationFieldHelpers = {

	updateFieldConfigurations(callback) {

        const {dispatch } = this.props;
        let fieldAreasContainerParams = getFieldAreaContainerBaseParameters();
        let fieldConfig = this.props.formationState.fieldConfig;
        let fieldConfigurations = _.defaultsDeep(fieldAreasContainerParams.fieldAreasConfigurations, fieldConfig);
        dispatch(Actions.prepareFieldConfig(fieldConfigurations))
        let self = this;

        let positionsConfigurations = {

            "accept": ".formation-spot",
            "drop": function(event, ui) {
                let area = $(event.target).attr("data-area");
                let spotId = $(ui.helper).attr("viewId");
                let currentPositionArea = $(ui.helper).attr("currentArea");

                let posX = ui.position.left;
                let posY = ui.position.top;

                posX = ( 100 * parseFloat(posX / parseFloat($(event.target).width())) ) ;
                posY = ( 100 * parseFloat(posY / parseFloat($(event.target).height())) ) ;

                if (currentPositionArea !== area) {
                   
                    dispatch(Actions.updateSpotArea(spotId, area, self.props.formationState.positions, self.props.formationState.fieldConfig))
                
                    let elementPosition = FormationFieldHelpers.calcElementPositionInArea.bind(self)(ui, $(this));
                    posX = elementPosition.posX
                    posY = elementPosition.posY
                }

                $(ui.helper).css("left", posX);
                $(ui.helper).css("top", posY);

                FormationFieldHelpers.updateSpotCoordinates.bind(self)(spotId, {
                    x: posX,
                    y: posY
                })

            }
        }
        callback(fieldConfigurations, positionsConfigurations);
    },

    calcElementPositionInArea(element, context) {
        var parent = element.draggable.parent();
        var draggedElement = $(element.draggable);
        var leftOffset = Math.abs(parent.offset().left - $(context).offset().left);
        var topOffset = $(context).offset().top - parent.offset().top;
        draggedElement.detach().appendTo($(context));
        let posX = draggedElement.position().left - leftOffset;
        let posY = draggedElement.position().top - topOffset;

        posX = ( 100 * parseFloat(posX / parseFloat($(parent).width())) ) ;
        posY = ( 100 * parseFloat(posY / parseFloat($(parent).height())) ) ;

        return {
            posX,
            posY
        };
    },

    updateSpotCoordinates(viewId, coordinates) {
        const {dispatch} = this.props;

        if (!viewId || !coordinates) {
            return
        }
        dispatch(Actions.updateSpotCoordinates(this.props.formationState.positions, viewId, {
            x: coordinates.x,
            y: coordinates.y,
        }));
    },

   
    getAreasHeight(areas)  {
        return areas.map((d, w) => d.height()).reduce((z, t) => {
            return z + t
        })
    },

    reCalcuateClosestFieldAreasDimensions(currentAreaIdex, options)  {
        let closestAreas = options.fieldAreas.filter((z, w) => w !== currentAreaIdex);
        let halfHeightOffset = (options.fieldAreasContainerParams.containerHeightPercentage - options.fieldAreaHeight) / closestAreas.length;

        closestAreas.forEach((w, z) => {
            let newHeight = halfHeightOffset;
            $(w).css({
                height: newHeight + '%'
            })
        })
    },

    getPositionsByArea(fieldAreas = null , playerPositions = null) {
    	fieldAreas = fieldAreas ? fieldAreas : this.props.formationState.fieldConfig.areas;
    	playerPositions = playerPositions ? playerPositions : this.props.formationState.positions;

        let defaultAreas = _.clone(fieldAreas);
        let positions = _.clone(playerPositions)
        let orderedPositions = _.orderBy(positions, ['order'], ['asc']);
        let formationPositionsByArea = _.groupBy(orderedPositions, 'area')

        defaultAreas = _.mapValues(defaultAreas, (e, i) => {
            let area = _.clone(e);
            return { ...area,
                positions: (formationPositionsByArea[i] ? formationPositionsByArea[i] : [])
            }
        })

        return defaultAreas;
    }
}

export const getFieldAreaContainerBaseParameters = () => {
	
    const fieldHeight =  CanvasHelpers.getCanvasDefaultHeight($(".field-wrapper").outerWidth() );
    const golkeeperAreaPercentage = 30
    const golkeeperAreaHeight = 	golkeeperAreaPercentage*fieldHeight / 100

    const containerHeightPercentage = 100 - golkeeperAreaPercentage;
    const containerHeight = fieldHeight - golkeeperAreaHeight;
    const baseFieldHeight = (containerHeightPercentage / 3 ).toFixed(2);
    const baseAreaHeight =  containerHeight/3;

    const fieldAreasConfigurations = {
        areas: {
            attacking: {
                element: 'field-attacking-area',
                style: {
                    height: baseFieldHeight + "%"
                },
                className: "attacking-area",
                name: "attacking",
                order: 1
            },
            midfield: {
                element: 'field-midfield-area',
                style: {
                    height: baseFieldHeight + "%",
                },
                className: "midfield-area",
                name: "midfield",
                order: 2
            },
            defensive: {
                element: 'field-defensive-area',
                style: {
                    height: baseFieldHeight + "%"
                },
                className: "defensive-area",
                name: "defensive",
                order: 3
            },
            goalkeeper: {
                style: {
                    height: golkeeperAreaPercentage + "%"
                },
                className: "goalkeeper-area",
                order: 4
            }
        }
    }

    return {
        golkeeperAreaPercentage,
        golkeeperAreaHeight,
        containerHeightPercentage,
        fullContainerHeight: fieldHeight,
        containerHeight,
        baseFieldHeight,
		fieldAreasConfigurations,
		baseAreaHeight: baseAreaHeight
    };
}