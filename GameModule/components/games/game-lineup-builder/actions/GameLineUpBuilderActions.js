import Constants from '../constants/GameLineUpBuilderConstants'
import { FormationFieldHelpers } from "../../../formations/formation-builder/containers/Field"
import { getFieldAreaContainerBaseParameters} from "../../../formations/formation-builder/containers/Field"

const Actions = {
    changePlayerPosition(viewId, playerId, positions, bench) {

        let spot = Helpers.findPositionSpotInField(viewId, positions);

        spot = spot.length > 0 ? spot[0] : null;

        if (!spot) return;

        spot.playerId = playerId;
        let newBench = Helpers.getPlayerOutOfBench(bench, spot.playerId);

        return dispatch => {
            dispatch({
                type: Constants.CHANGE_POSITION,
                positions: positions,
                bench: newBench
            })
        }
    },
    changeLineupName(name) {
        return dispatch => {
            dispatch({
                type: Constants.CHANGE_NAME,
                name: name
            })
        }
    },
    showText(text, type) {
        return dispatch => {
            dispatch({
                type: Constants.SHOW_TEXT,
                text: text,
                textType: type
            })
        }
    },
    showInjuredPlayersWarning(show) {
        return dispatch => {
            dispatch({
                type: Constants.SHOW_INJURED_PLAYERS_WARNING,
                show: show
            })
        }
    },
    saveLineup(url, saveData) {
        return dispatch => {

            dispatch({
                type: Constants.SAVING,
                saving: true,
                warningText: {
                    text: "",
                    type: "SUCCESS"
                }
            })
            ajaxCall({
                ajax: {
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        data: JSON.stringify(saveData)
                    },
                    success: function(data) {
                        if (data.ok) {
                            dispatch({
                                type: Constants.SAVING,
                                saving: false,
                                warningText: {
                                    text: data.msg,
                                    type: "SUCCESS"
                                }
                            })
                        } else {
                            dispatch({
                                type: Constants.SAVING,
                                saving: false,
                                warningText: {
                                    text: data.msg,
                                    type: "DANGER"
                                }
                            })
                        }
                    }
                }
            })

        }
    },
    loadSavedPositions(formation, positions) {
        let positionsByArea = Helpers.getPositionsByArea(positions)
        return dispatch => {
            dispatch({
                type: Constants.LOAD_SAVED_POSITIONS,
                positions: positionsByArea,
                template: formation
            })
        }
    },
    loadFormation(id, positions, gameSelection) {

        return (dispatch, getState) => {
            let state = getState().lineUpState;
            let currentFormation = state.formation ? state.formation : null;

            let bench = state.bench;
            /* Formation changed, everybody to the bech and reset positions */
            if (currentFormation && currentFormation != id) {

                bench = gameSelection.map((e, i) => {
                    return e.id;
                });
            }

            let positionsByArea = Helpers.getPositionsByArea(positions)

            dispatch({
                type: Constants.LOAD_FORMATION,
                positions: positionsByArea,
                template: id,
                bench: bench
            })
        }
    },
    resetBench() {
        return (dispatch, getState) => {
            let state = getState().lineUpState;
            let gameSelection = _.clone(state.mainConfig.gameSelection);

            let bench = gameSelection.map((e, i) => {
                return e.id;
            });

            const formation = state.mainConfig.formations.filter((e, i) => {
                return e.id == state.formation
            })[0];
            let positions = Helpers.mapSpots(formation.player_positions);

            let positionsByArea = Helpers.getPositionsByArea(positions)

            dispatch({
                type: Constants.LOAD_FORMATION,
                positions: positionsByArea,
                template: state.formation,
                bench: bench
            })
        }
    },
    prepareBench(newBench) {
        return dispatch => {
            dispatch({
                type: Constants.LOAD_BENCH,
                bench: newBench
            })
        }
    },
    swapPlayerPositions(positions, viewIdSource, viewIdTarget) {

        let sourceSpot = Helpers.findPositionSpotInField(viewIdSource, positions);
        let copySource = _.clone(sourceSpot[0]);
        let targetSpot = Helpers.findPositionSpotInField(viewIdTarget, positions);

        let copyTarget = _.clone(targetSpot[0]);

        targetSpot[0].playerId = copySource.playerId;
        sourceSpot[0].playerId = copyTarget.playerId;

        return dispatch => {
            dispatch({
                type: Constants.REFRESH_POSITIONS,
                positions: positions
            })
        }
    }
}

export const Helpers = {
    findPositionSpotInField(spotId, positionsByArea) {
        let positions = positionsByArea.map((e, i) => e.positions).reduce((p1, p2) => [...p1, ...p2])
        let sourceSpot = positions.filter((e) => {
            return e.viewId == spotId
        });
        return sourceSpot;
    },
    fixPositions(positions, fix = 120 / 4) {
        return positions.map((e, i) => {
            e.coordinates.x = e.coordinates.x - (fix);
            return e;
        })
    },
    getPositionsByArea(positions) {
        let newPositions = positions //Helpers.fixPositions(positions);
        let positionsByArea = _.values(FormationFieldHelpers.getPositionsByArea(getFieldAreaContainerBaseParameters().fieldAreasConfigurations.areas, newPositions));
        return positionsByArea;
    },
    getPlayerOutOfBench(bench, playerId) {
        return _.remove(bench, function(e) {
            return e == playerId;
        });
    },
    returnPlayerToBench(bench, playerId) {
        return bench.push(playerId);
    },
    insertPlayerToBench(bench, playerId, position) {
        bench[position] = playerId
        return bench;
    },
    mapSpots(formationPositions, players = null) {

        let playerId;
        players = players || [];

        return formationPositions.map((e) => {

            let spot = players.filter((p, i) => {
                return p.spot_view_id == e.view_id
            })[0]
            playerId = spot ? (_.isObject(spot.player) ? spot.player.id : spot.player) : null;

            return {
                playerId: playerId,
                viewId: e.view_id,
                style: Helpers.calcSpotPosition(e.x, e.y, e.area),
                area: e.area
            }
        })
    },
    calcSpotPosition(x, y, area) {
        let spotAreaBaseWidth = 800;
        let areaBaseParameters = getFieldAreaContainerBaseParameters();
        let spotAreaBaseHeight = area !== "goalkeeper" ? areaBaseParameters.baseAreaHeight : areaBaseParameters.golkeeperAreaHeight;
        let spotTop = area !== "goalkeeper" ? ((100 * y) / spotAreaBaseHeight) + 20 + "%" : 0

        return {
            left: x - (8 / 2) + "%",
            top: y + "%",
            position: "absolute",
            width: "14%",
            height: area !== "goalkeeper" ? "56%" : "44%"
        }

    }
}

export default Actions