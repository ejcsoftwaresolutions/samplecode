import Constants from '../constants/GameLineUpBuilderConstants'
import { Helpers } from '../../../formations/utils/canvas-helpers/CanvasHelper';

export const initialState = {
 	canvas:  Helpers.createCanvas(),
 	name: '',
    formation: null,
    id: null,
    gameLineup: null,
    defaultView: null,
    saving: false,
    warningText: {},
    showInjuredPlayersWarning: false,
 	positions: [],
 	mainConfig: {},
    bench: []
}

export default function LineUpReducer(state = initialState, action) {
    switch (action.type) {
    	case  Constants.CHANGE_POSITION:
    		return {...state, positions: [...action.positions] }
    		break;
        case  Constants.CHANGE_SUBSTITUTE_PLAYER_POSITION:
            return {...state, positions: [...action.positions] }
            break;
        case  Constants.LOAD_SAVED_POSITIONS:
            return {...state, positions: [...action.positions], formation: action.template }
            break;
    	case  Constants.CHANGE_NAME:
    		return {...state, name: action.name}
    		break;
        case Constants.LOAD_FORMATION:
            return {...state, positions: [...action.positions], formation: action.template, bench: action.bench, positions: action.positions }
            break;
        case Constants.SAVING:
            return {...state, saving: action.saving,  warningText:  action.warningText }
            break;
        case Constants.SHOW_TEXT:
            return {...state, warningText: {  text: action.text, type: action.textType } }
            break;
        case Constants.SHOW_INJURED_PLAYERS_WARNING:
            return {...state, showInjuredPlayersWarning: action.show }
            break;
        case Constants.LOAD_BENCH:
            return {...state, bench: action.bench }
            break;
        case Constants.REFRESH_POSITIONS:
            return {...state, positions: action.positions }
            break;
	    default:
	        return state
    }
}
