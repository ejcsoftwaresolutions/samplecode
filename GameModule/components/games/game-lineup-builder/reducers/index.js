import LineUpReducer from './LineUpReducer'

import { initialState as lineUpState } from './LineUpReducer'

import { combineReducers }  from 'redux'

// Combine all reducers you may have here
export default combineReducers({
    lineUpState: LineUpReducer,
    
})

export const initialStates = {
    lineUpState,
}
