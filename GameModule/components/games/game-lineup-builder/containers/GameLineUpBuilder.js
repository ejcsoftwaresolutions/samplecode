import React from 'react'
import Actions from '../actions/GameLineUpBuilderActions'
import { connect } from 'react-redux'
import GameLineUpBuilderTemplate from  '../templates/GameLineupBuilderTemplate.rt'
import {getFieldAreaContainerBaseParameters} from "../../../formations/formation-builder/containers/Field"
import {Helpers} from '../actions/GameLineUpBuilderActions'
import { Provider } from 'react-redux'

class GameLineUpBuilder extends React.Component {

	constructor(props, context) {
		super(props, context);	
		this.state = {
			loading: false,
		}
	}

	componentDidMount() {
		this.loadField()
		this.loadEvents();
	}

	loadField() {
		const {dispatch} = this.props;
		let bench;

		if(this.props.lineUpState.id) {
			bench  = this.props.lineUpState.gameLineup.lineup_substitutes.map((e,i) => {
	        	return e.player.id;
	      	})
	      	bench = _.uniq(bench);
	      	
      		this.loadSavedPositions(this.props.lineUpState.gameLineup);

		} else {
		  	bench = this.props.lineUpState.mainConfig.gameSelection.map((e,i) => {
		        return e.id;
	      	})
		}

    	dispatch(Actions.prepareBench(bench));
	}

	loadEvents() {
		let templateSelect = this.refs.templateSelect;
		

		$(templateSelect).select2({
            "minimumResultsForSearch": Infinity
		}).on("change", (e) => {
			this.handleLoadFormation(e);
		})
	}

	fetchPreviousLineup(e) {
		let gameId = $(e.currentTarget).val();

		if(!gameId) return false;

		this.setState({
			loading: true
		});

	    ajaxCall({
          ajax: {
            url: this.props.mainConfig.extractLineupFromGameUrl,
			type: 'POST',
			dataType: 'json',
			data: { id: gameId  } ,
            success: (data) => {
            	this.setState({
	             	loading: false
	         	})
	     	 
	 	 	 	this.loadPreviousLineup(data)   
            }
           }
        })
      
	}

	loadPreviousLineup(data) {
		const {dispatch} = this.props;

		/* Cleaning container */
 		$(".spot-lines-container").html("");

 	 	let currentGameSelection = this.props.mainConfig.gameSelection.map((e,i) => {
 	 		return  {
 	 	 		id: e.id,
 	 	 		name: e.name	
 	 	 	} 
 	 	})
 	 	/* Bench + playersinfield */
 		let previousGameSelection =  _.union(
			data.game_lineup.lineup_player_positions.map((e,i) => {
     	 	 	return  {
     	 	 		id: e.player.id,
     	 	 		name: e.player.name	
     	 	 	} 
	 	 	}),
	 	 	data.game_lineup.lineup_substitutes.map((e,i) => {
     	 	 	return {
     	 	 		id: e.player.id,
     	 	 		name: e.player.name	
     	 	 	} 
	 	 	})
 	 	);
 
 		/* Players not select for this game but were playing in the previous one*/
 		let abscentFromSelection =  _.differenceBy(previousGameSelection, currentGameSelection, 'id');

		if(abscentFromSelection.length > 0) {
			const absentPlayersNames =abscentFromSelection.map((e) => { return e.name }).join(', ');

			dispatch(Actions.showText(Translator.trans('lineup.missing.players.in.selection', {'playersNames': absentPlayersNames }, 'lineup'), 'WARNING' ));

			/* Making available new spots */
			let newPositions = data.game_lineup.lineup_player_positions.map((e,i) => {

				if(_.indexOf(abscentFromSelection.map((z) => { return z.id}) , e.player.id ) >= 0) {
					e.player = null;
				}

				return e;
			})

			data.game_lineup.lineup_player_positions = newPositions; 

		}

		let playersInField = _.filter(data.game_lineup.lineup_player_positions, (e, i)=> {
			return !_.isNull(e.player);
		}).map((e,i) => {
 	 	 	return e.player.id;
 	 	})

 		
 	 	/* Players from game selection who are not in field*/
 		let bench =  _.differenceBy(currentGameSelection, playersInField.map((id, i) => {
 			return {
 				id: id
 			}
 		}), 'id').map((e) => { return e.id });


     	this.loadSavedPositions(data.game_lineup);
		dispatch(Actions.prepareBench(bench));
	}

	validPlayerFormation(validateInjuriesPlayers) {
		let lineupPositions = this.getPositionSpots();
		let isValid = true;
		let showInjuredPlayersWarning = false;
		let warningText =  "";
		let {trans} = this.props.mainConfig;
		
		if(lineupPositions.length === 0) {
			warningText =  trans.validation.noFormationSelected;
			isValid = false;
		}

		for (var i = 0; i < lineupPositions.length; i++) {
			if(!lineupPositions[i].playerId) {
				warningText = trans.validation.missingPlayers;
				isValid = false;
				break;
			} 
		}

		if(this.duplicatedPlayers())  {
			warningText =  trans.validation.duplicatedPlayers;
			isValid = false;
		}
		let injuredPlayers = this.props.mainConfig.gameSelection.filter((e) => {
			let includes = this.getPositionSpots().map((p) => { return parseInt(p.playerId) })
			return _.includes( includes, parseInt(e.id) ) && parseInt(e.is_injured)
		}).length > 0;

		if(validateInjuriesPlayers &&  injuredPlayers ) {
			isValid = false;
			showInjuredPlayersWarning =  true;
		}
	
		return {
			isValid,
			warningText,
			showInjuredPlayersWarning
		}
	}

	duplicatedPlayers() {
		let mainPlayers =  this.getPositionSpots().map((e) => {return e.playerId});
		let substitutePlayers =  this.getPositionSpots().map((e) => {return parseInt(e.substitutePlayerId) }).filter((e) => {
			return !_.isNull(e) || !_.isNaN(e)
		});
		substitutePlayers = substitutePlayers.filter(function(e) { return !isNaN(e) })
		let allPlayers = mainPlayers.concat(substitutePlayers);

		return _.filter(allPlayers, function (value, index, iteratee) {
		   return !_.isNull(value) && _.includes(iteratee, value, index + 1);
		}).length > 0;

	}

	saveLineup(validateInjuriesPlayers, e) {
		const {dispatch} = this.props;
		const playerFormationValidation = this.validPlayerFormation(validateInjuriesPlayers);
	
		if(playerFormationValidation.isValid) {
			dispatch(Actions.showInjuredPlayersWarning(false));	
			$(e.currentTarget).attr("disabled", "disabled")
			let currentLineupState = this.props.lineUpState;
			let saveData = {
				formation: currentLineupState.formation,
				id: currentLineupState.id,
				gameId: currentLineupState.gameId,
				gameLineup: currentLineupState.gameLineup,
				positions: this.getPositionSpots(),
				bench: currentLineupState.bench
			}
			dispatch(Actions.saveLineup(this.props.mainConfig.saveUrl,  saveData, this.props.mainConfig.gameEditUrl));	
		} else {
			if(!playerFormationValidation.showInjuredPlayersWarning) {
				
				dispatch(Actions.showText(playerFormationValidation.warningText, "DANGER"));	
			} else {
				dispatch(Actions.showInjuredPlayersWarning(true));	
			}
		}
		Helpers.fixBenchPlayersPositions();
	}

	getPositionSpots() {
  	    let positions =  this.props.lineUpState.positions.map((e, i) => e.positions).reduce((p1, p2) => [...p1, ...p2] )
		return positions
	}

	loadSavedPositions(gameLineup){
		const formation = this.props.mainConfig.formations.filter((e, i) => { return e.id == gameLineup.formation.id })[0];
		const {dispatch} = this.props;

		if (!formation) return;
		let positions =  Helpers.mapSpots(formation.player_positions,  gameLineup.lineup_player_positions);

		$(this.refs.templateSelect).val(formation.id).trigger('change');
    	dispatch(Actions.loadSavedPositions(formation.id, positions));
	}

	handleLoadFormation(e) {
		const value =  e.currentTarget.value;
		const formation = this.props.mainConfig.formations.filter((e, i) => { return e.id == value.trim() })[0];
		if (!formation  || !formation.player_positions) return;

		const {dispatch} = this.props;
		let positions = Helpers.mapSpots(formation.player_positions);

    	dispatch(Actions.loadFormation(formation.id, positions, this.props.mainConfig.gameSelection));
    	Helpers.fixBenchPlayersPositions();
	}	

	getPlayerPosition(id) {
		const position = this.props.mainConfig.playerPositions.filter((e, i) => { return e.id == id })[0];
		return position;
	}

	selectPlayer(e) {
		let data =  { playerId:  e.currentTarget.value, positionId:  e.currentTarget.getAttribute("data-position-id"), viewOrder: e.currentTarget.getAttribute("data-view-order") };
		const {dispatch} = this.props;
		let indicatorContainer = $(e.currentTarget).closest('.lineup-position-input-group').find('.player-injured-indicator-container');
		this.checkInjuredPlayer(data.playerId, indicatorContainer);
		dispatch(Actions.changePlayerPosition(this.props.lineUpState.canvas, this.getPositionSpots(), data.positionId, data.viewOrder, data.playerId, this.props.mainConfig.gameSelection));
	}

	checkInjuredPlayer(playerId, indicatorContainer) {
		let playerInfo = this.props.mainConfig.gameSelection.filter((e) => { return  e.id == playerId });
		if(playerInfo.length > 0) {
			playerInfo = playerInfo.shift();
			
			if(parseInt(playerInfo.is_injured) ) {
				indicatorContainer.removeClass('hidden')
			} else {
				indicatorContainer.addClass('hidden')
			}
		}
	}
	
	getPlayerInfo(id) {
		let result = this.props.mainConfig.gameSelection.filter((e) => {
			return e.id == id;
		})
		return result.length > 0 ? result[0] : null;
	}

	hideWarningMessage() {
		const {dispatch} = this.props;
		dispatch(Actions.showText("", "SUCCESS"));	
	}

	render() {
		setTimeout(() => {

			let selectPrevious = this.refs.loadPrevious;

			$(selectPrevious).select2entity().on("change", (e) => {
				this.fetchPreviousLineup(e);
			})
		},0)
	  return GameLineUpBuilderTemplate.apply(this);
	}

}

const mapStateToProps = (state) => (
{
	"lineUpState" : state.lineUpState,
	"mainConfig": state.lineUpState.mainConfig,
})

GameLineUpBuilder = connect(mapStateToProps)(GameLineUpBuilder)

const mainNode = () => {
    const store = ReactOnRails.getStore('GameLineUpBuilderStore')

    const reactComponent = (
        <Provider store={store}>
               <GameLineUpBuilder />
        </Provider>
    )
    return reactComponent
}

export default mainNode
