import React from 'react'
import { connect } from 'react-redux'
import PlayerSpotTemplate from '../templates/PlayerSpotTemplate.rt'
import Actions from '../actions/GameLineUpBuilderActions'
import {findDOMNode} from 'react-dom'
import {Helpers} from '../actions/GameLineUpBuilderActions'

class PlayerSpot extends React.Component {

	constructor(props) {
		super(props);
		this.state ={
			showPlayerChooser: false
		}
	}

  	componentDidMount() {
  		this.initDropEvents();
	 
	}

	initDropEvents() {
		let instance  = this;
	  	$(findDOMNode(this)).droppable({
 		 	accept: function(d) { 
		        if(d.hasClass("player-element") || d.hasClass("player-on-field-element")){ 
		            return true;
		        }
		    },
  		 	"drop": ( event, ui ) =>  {
				let target = $(event.target);
				let source = $(ui.helper);
	 		 	let viewIdTarget = target.find(".spot").attr('spotId');
				let bench;
				
  		 		let playerId = source.attr("playerId");
	 			bench = instance.props.lineUpState.bench;

  		 		$(ui.draggable).remove()

  		 		if( target.find(".lineup-player-placed-spot").length > 0) {
  		 			if(source.hasClass("player-on-bench")) {
  		 				let returnPlayerId = target.find(".spot").attr('playerId');
  		 				bench = Helpers.insertPlayerToBench(bench, parseInt(returnPlayerId), _.indexOf(bench, parseInt(playerId)) )
  		 			} else if (source.hasClass("lineup-player-placed-spot")) {
  		 				let viewIdSource = source.attr("spotId");
  		 				instance.swapPositionsAction(viewIdSource, viewIdTarget );
  		 			}

  		 		} 

  		 		instance.addToField(viewIdTarget, playerId, bench);
	      	}
	  	});
	}

	componentDidUpdate(prevProps, prevState) {
		/* Make draggable to all new placed spots */
		$(".lineup-player-placed-spot").not(".ui-draggable").draggable({
	  		"revert": "invalid",
	  		"helper": 'clone',
	  		"classes": {
			    "ui-draggable": "player-on-field-element"
		  	}
  		});
	}
	
	swapPositionsAction(viewIdSource, viewIdTarget) {
		const {dispatch} = this.props;
		
  		dispatch(Actions.swapPlayerPositions(this.props.lineUpState.positions, viewIdSource, viewIdTarget));
	}

	addToField(spotId, playerId, bench) {
  		const {dispatch} = this.props;
  		dispatch(Actions.changePlayerPosition(spotId, playerId, this.props.lineUpState.positions, bench));
  	}

	onSpotClick() {
		this.setState(state => {
			state.showPlayerChooser = state.showPlayerChooser ? false : true;
			return state;
		})
	}

	onChoosePlayer(e) {
		const {dispatch} = this.props;

		let playerId = e.currentTarget.value;
		let viewId = $(e.currentTarget).closest(".player-spot").attr("spot-id");

		this.setState({
			showPlayerChooser:false 
		})
		dispatch(Actions.changePlayerPosition(viewId, playerId, this.props.lineUpState.positions, this.props.lineUpState.bench));
		
	}

	render() {
		return PlayerSpotTemplate.apply(this);
	}
}


const mapStateToProps = (state) => (
    {
	   	"lineUpState" : state.lineUpState,
		"mainConfig": state.lineUpState.mainConfig,
    }
)



export default connect(mapStateToProps)(PlayerSpot)  ;

