import React from 'react'
import Actions from '../actions/GameLineUpBuilderActions'
import { connect } from 'react-redux'
import PlayerOnBenchTemplate from  '../templates/PlayerOnBenchTemplate.rt'
import {Helpers} from '../actions/GameLineUpBuilderActions'
import {findDOMNode} from 'react-dom'

class PlayerOnBench extends React.Component { 

	constructor(props) {
		super(props);
	}

  	componentDidMount() {

	  	$(findDOMNode(this)).draggable({
	 		"revert": "invalid",
	 		
  			start: function(event, ui){
				event.stopPropagation();

  				$(this).addClass("dragging")
			   
			},
			stop: function(event, ui){
				event.stopPropagation();
			 	$(this).removeClass("dragging")
			}
	  	});
	}

  
  	render() {
  		return PlayerOnBenchTemplate.apply(this);
  	}
}

const mapStateToProps = (state) => (
{
	"lineUpState" : state.lineUpState,
	"mainConfig": state.lineUpState.mainConfig,
})

export default connect(mapStateToProps)(PlayerOnBench) ;

