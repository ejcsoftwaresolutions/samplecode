import React from 'react'
import Actions from '../actions/GameLineUpBuilderActions'
import { connect } from 'react-redux'
import BenchTemplate from  '../templates/BenchTemplate.rt'
import {Helpers} from '../actions/GameLineUpBuilderActions'

class Bench extends React.Component { 

	constructor(props) {
		super(props);
	}

	resetBench() {
		const {dispatch} = this.props;

    	dispatch(Actions.resetBench());
		Helpers.fixBenchPlayersPositions();
	}

	componentDidUpdate() {
	}

  	render() {
  		$(".lineup-bench").html('');
  		return BenchTemplate.apply(this);
  	}
}


const mapStateToProps = (state) => (
{
	"lineUpState" : state.lineUpState,
	"mainConfig": state.lineUpState.mainConfig,
})

export default  connect(mapStateToProps)(Bench) ;