import React from 'react'
import Actions from '../actions/GameLineUpBuilderActions'
import { connect } from 'react-redux'
import {Helpers} from  '../../../formations/utils/canvas-helpers/CanvasHelper'
import FieldTemplate from  '../templates/FieldTemplate.rt'

class Field extends React.Component { 

	constructor(props) {
		super(props);
	}


  	render() {
  		return FieldTemplate.apply(this);
  	}
}


const mapStateToProps = (state) => (
{
	"lineUpState" : state.lineUpState,
	"mainConfig": state.lineUpState.mainConfig,
})

export default  connect(mapStateToProps)(Field) ;