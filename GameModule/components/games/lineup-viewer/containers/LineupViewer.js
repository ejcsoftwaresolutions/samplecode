import React from 'react'
import { Helpers as GameLineUpHelpers } from '../../../../helpers/GameLineupHelpers';
import LineupViewerTemplate from  '../templates/LineupViewer.rt'
import {FormationFieldHelpers} from  '../../../../helpers/FormationFieldHelpers';
import {getFieldAreaContainerBaseParameters} from  '../../../../helpers/FormationFieldHelpers';

import Styles from '../../../../styles/lineup.scss';

class LineupViewer extends React.Component {

	constructor(props, context) {
		super(props, context);	
	}


	componentDidMount() {
		if(this.props.gameLineup) {

			let positions = this.getPositionsByFieldArea();
			this.setState({
				lineupPositions: positions,
				substitutions: this.props.gameLineup.lineup_substitutes.map((e, i) => {
					return e.player;
				})
			})
		}
	}

	getPositionsByFieldArea() {
		let configuredPositions = this.configurePlayerPositions();
		let positionsByArea = FormationFieldHelpers.getPositionsByArea(getFieldAreaContainerBaseParameters().fieldAreasConfigurations.areas, configuredPositions);
		return _.values(positionsByArea);
	}

	configurePlayerPositions() {
		let positions =  this.props.gameLineup.lineup_player_positions;
		let lineupPositions = this.props.gameLineup.formation.player_positions.map((e, i) => {
			let matchPosition = positions.filter((a) => {  return a.spot_view_id == e.view_id  })
			matchPosition = matchPosition ? matchPosition.shift() : null;
			return {
				viewId: e.view_id,
				style: GameLineUpHelpers.calcSpotPosition(e.x, e.y, e.area),
				player: matchPosition.player,
				area: e.area,
			}
		});

		return lineupPositions
	}

	deleteLineup() {
		let modalElement = $("#deleteLineupModal");
		let idItem = this.props.gameLineup.id;

	  	modalElement.modalPrompt({
            title: Translator.trans("coachy.delete.msg", {'elementName': Translator.trans("coachy.lineup", {}, 'general').toLowerCase()}, "general"),
            onAccept: (btn, modalProps) => {
                ajaxCall({
                    btn: btn,
                    ajax: {
                        url: this.props.routes.deleteLineup,
                        data: {
                            id: idItem
                        },
                        success: function(data) {
                            if (data.ok) {
                                modalElement.modal('hide');
                                
                            } else {
                              
                            }
                        }
                    }
                })
            }
        });
	}

	render() {
		return LineupViewerTemplate.apply(this);
	}

}
LineupViewer.defaultProps = {
	canvas: null,
	name: '',
	id: null,
	positions: [],
	canvasEvents: {},
	gameLineup: null,
	formation: [],
	mainConfig: {
		fields: {
		  	full: "img/football_field_vertical_translated.png",
            half: "img/half_field.PNG" ,
		}
	}
}


export default LineupViewer