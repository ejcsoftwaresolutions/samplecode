import GameResultReducer from './GameResultReducer'
import GameTimelineReducer from '../../game-timeline/reducers/GameTimelineReducer'

import { initialState as gameResult } from './GameResultReducer'
import { initialState as timeline} from '../../game-timeline/reducers/GameTimelineReducer'

import { combineReducers }  from 'redux'

// Combine all reducers you may have here
export default combineReducers({
    gameResult: GameResultReducer,
    gameTimeline: GameTimelineReducer
})

export const initialStates = {
    gameResult,
    timeline
}
