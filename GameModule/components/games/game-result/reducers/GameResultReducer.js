import Constants from '../constants/GameResultConstants'

export const initialState = {
	homeTeamResult: 0,
	awayTeamResult: 0,
	saving: false,
	gameResultDetails: {
		homeTeam: [],
		awayTeam: [],
	},
	originalResultDetails: {
		homeTeam: [],
		awayTeam: [],
	},
	gameReportEntries: [],
	editMode: false,
}

export default function GameResultReducer(state = initialState, action) {
    switch (action.type) {
		case Constants.SAVING:
			return {...state, saving: action.state }
		break;
		case Constants.CHANGE_TEAM_RESULT:
			return {...state, ...action.gameResult }
		break;
		case Constants.CHANGE_TEAM_DETAILS: 
			return {...state, gameResultDetails: action.gameResultDetails }
		case Constants.CHANGE_ENTRIES_REFERENCES:
			return {...state, gameResultDetails: action.gameResultDetails,  originalResultDetails: action.originalResultDetails  }
		break;
		case Constants.UPDATE_GAME_TIMELINE_ENTRIES:
			return {...state, gameReportEntries: action.gameReportEntries, saving:false}
		break;
		case Constants.UPDATE_GAME:
			let mainConfig = state.mainConfig;
			mainConfig.game = action.game;
			return {...state, mainConfig: mainConfig}
		break;
		
	    default:
	        return state
    }
}
