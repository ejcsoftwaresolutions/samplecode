import React from 'react'
import { connect } from 'react-redux'
import GameResultTemplate from '../templates/GameResultTemplate.rt';
import Actions from '../actions/GameResultActions'

class GameResult extends React.Component {

	constructor(props, context) {
		super(props, context);	
			
		this.state = {
			loading: false,
			linesEdited: false
		}

		this.mapGoalEntries(props);
	}

	componentWillReceiveProps(nextProps) {

	}

	mapGoalEntries(props, gameGoals = null) {
		let newGameGoals = !gameGoals ?  
				{...props.gameResult.sharedFunctions.getGoalsFromEntries(props.gameResult.gameReportEntries, false) }
				: {...gameGoals};
		
		newGameGoals = {...newGameGoals};
		if(props.mainConfig.game.is_home_game) {
			newGameGoals["awayTeam"] =  newGameGoals.awayTeam.map((e) => { let item = _.clone(e);  item['player'] = item.opponent_player; delete item.opponent_player; return item; });
		} else {
			newGameGoals["homeTeam"] =  newGameGoals.homeTeam.map((e) => { let item = _.clone(e);  item['player'] = item.opponent_player; delete item.opponent_player; return item; });
		}

		this.setState({
			gameResultDetails: { homeTeam: newGameGoals.homeTeam , awayTeam: newGameGoals.awayTeam },
			originalResultDetails: { homeTeam: newGameGoals.homeTeam.map((e) => { return e.id } ) , awayTeam: newGameGoals.awayTeam.map((e) => { return e.id } ) },
			linesEdited: false,
		})
		
	}

	componentDidMount() {
		this.initEvents();
	}

	componentDidUpdate(prevProps, prevState) {
		this.initEvents();
	}

	componentWillUpdate(nextProps, nextState) {
		if(!nextState.linesEdited) {
			this.mapGoalEntries(nextProps);
		}
	}

	initEvents() {
		$(".player-list").select2().off("change").on("change", this.onInputResultChange.bind(this));
	}

	onInputResultChange(e) {

		let value = e.currentTarget.value;
		let element = $(e.currentTarget);
		let inputType =  element.attr("data-input-type");
		let teamType =  element.attr("data-team-type");
		let goalNumber =  element.closest(".goal-result-row").attr("data-goal-key");
	
		
		this.changeResultGameDetail(inputType, value, goalNumber, teamType, this.state.gameResultDetails);
	}

	changeResultGameDetail(inputType, value, goalNumber, teamType, gameDetails) {
	
		let newGameDetails = _.clone(gameDetails);
		let detailInfo = { player: null, minute: null, game: this.props.mainConfig.game.id };
		let index = !_.isNaN(parseInt(goalNumber)) ? parseInt(goalNumber) : 0 ;
		let key =  inputType === "minute" ?  "minute" : "player";
		detailInfo[key] = value;

		if(newGameDetails[teamType][index]) {
			newGameDetails[teamType][index][key] = value;
		} else {
			newGameDetails[teamType][index] = detailInfo;
		}
		this.setState({gameResultDetails: newGameDetails, linesEdited: true})
		
	}

	saveResult(e) {

		let hasLineupSet = !_.isUndefined(this.props.mainConfig.game.game_lineup);
		
        let data = { 
        			hasDetails: this.state.gameResultDetails.homeTeam.length > 0 ||  this.state.gameResultDetails.awayTeam.length > 0,
        			originalResultDetails: this.state.originalResultDetails,
        			homeTeamResult: this.props.gameResult.homeTeamResult,
        			awayTeamResult: this.props.gameResult.awayTeamResult,
        			gameResultDetails: this.state.gameResultDetails,
        			id:  this.props.gameResult.mainConfig.game.id
    			} ;

       	const {dispatch} = this.props;
       
		dispatch(Actions.saveResult(this.props.mainConfig.saveUrl, data, true, (goalEntries) => {
		}));
	}

	changeTeamResult(teamType, e) {
		let value =  !_.isEmpty(e.currentTarget.value)  ? e.currentTarget.value : 0;
		const {dispatch} = this.props;
		dispatch(Actions.changeTeamResult(teamType, value));
	}

	updateTeamGoalsLines(teamType, requiredLinesCount, goalEntryLines) {
		
		requiredLinesCount =  parseInt(requiredLinesCount);
		let originalCount = goalEntryLines ? parseInt(goalEntryLines.length) : 0
		
		if( originalCount > requiredLinesCount) {
			let keepCount = requiredLinesCount - originalCount;
			goalEntryLines = goalEntryLines.slice(0, keepCount);
		} else {
			let restElements =  [...new Array(parseInt(Math.abs(requiredLinesCount - originalCount)))].map((e) => {
				return { player: null, minute: null, game: this.props.mainConfig.game.id }
			}) ;
			goalEntryLines = goalEntryLines.concat(restElements)
		}

		this.setState((state)=>{
			let gameResultDetails = state.gameResultDetails;
			gameResultDetails[teamType] = goalEntryLines;
			return {
				gameResultDetails: gameResultDetails,
				linesEdited: true
			}
		});
	}

	getPlayerInfo(id) {
		const gameLineup = this.props.mainConfig.game.game_lineup;

		if(!gameLineup) {
			return id
		}
		const playersInGame = gameLineup.lineup_player_positions.map((e) => { return e.player }).concat( gameLineup.lineup_substitutes.map((e) => { return e.player }) );
		let match = _.filter(playersInGame, (e) => { return parseInt(e.id) == parseInt(id) });
		match =  match.length > 0 ?  match.shift() : id;

		return match ;
	}

	getSortedTeamEventList(teamType) {
		let reportEntries = this.props.gameResult.gameReportEntries;
		let scoreEntries =  _.clone(this.state.gameResultDetails[teamType]) ;
		let baseAssetPath = this.props.mainConfig.baseAssetPath
		
		let isHomeGame = this.props.mainConfig.game.is_home_game;
		let scoreIcon =  baseAssetPath.replace("PATH", "img/icons/goal2.png");

		scoreEntries = scoreEntries.map((e) => {
			let item = _.clone(e);
			
			if(isHomeGame &&  teamType === "homeTeam" || !isHomeGame && teamType === "awayTeam" ) {
				item.player = this.getPlayerInfo(item.player).name;
				item.minute =  parseInt(item.minute)
			} 
			item["icon"] = scoreIcon
			return item
		})

		let cardEntries = [];

		if( isHomeGame &&  teamType === "homeTeam" || !isHomeGame && teamType === "awayTeam") {
			cardEntries =  reportEntries.filter((e) => {  return e.type === "card" } );
			cardEntries = cardEntries.map((e) => {
				return {
					id: e.id,
					minute: parseInt(e.minute),
					icon: e.cardType === 1 ? baseAssetPath.replace("PATH", "img/icons/yellow-card.png") : baseAssetPath.replace("PATH", "img/icons/red-card.png"),
					player: this.getPlayerInfo(e.player).name
				}
			})
		} 

		return _.sortBy(scoreEntries.concat(cardEntries), ["minute"] ) ;
	}

	getLineupPlayersView(teamType, player, e) {

		const gameLineup = this.props.mainConfig.game.game_lineup;
		if(!gameLineup) {
			return (<div> </div>) 
		}
	
		const playersInGame = gameLineup.lineup_player_positions.map((e) => { return e.player }).concat( gameLineup.lineup_substitutes.map((e) => { return e.player }) )

		let selectedPlayer = _.head(this.state.gameResultDetails[teamType].filter((e) => { return e.player == player}));
			
		let options = playersInGame.map((e) => {  
			if(e.id == selectedPlayer.player) {
				return (<option selected  value={e.id} > {e.name} </option>) 
			} else {
				return (<option value={e.id} > {e.name} </option>) 
			}
		} )

		return (
			<select data-team-type={teamType} data-input-type="player"  className="form-control player-list">
				{
					!selectedPlayer.player ?
					<option selected> </option>
					:
					<option> </option>
				}
				
				{options}
			</select>
		)
	}

    render() {
    	
        return (
        	GameResultTemplate.apply(this)
        )
    }

}

const mapStateToProps = (state) => (
{
	mainConfig: state.gameResult.mainConfig,
	gameResult: state.gameResult,
	editMode: state.gameResult.editMode,
})

export default connect(mapStateToProps)(GameResult)
