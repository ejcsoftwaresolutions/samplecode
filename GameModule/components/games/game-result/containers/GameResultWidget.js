import React from 'react'
import { connect } from 'react-redux'
import GameResultWidgetTemplate from '../templates/GameResultWidgetTemplate.rt';
import { Provider } from 'react-redux'

class GameResultWidget extends React.Component {

	constructor(props, context) {
		super(props, context);	
	}

    render() {
        return GameResultWidgetTemplate.apply(this)
    }

}

const mapStateToProps = (state) => (
{
	mainConfig: state.gameResult.mainConfig,
	gameResult: state.gameResult,
	editMode: state.gameResult.editMode,
})

GameResultWidget = connect(mapStateToProps)(GameResultWidget)

const mainNode = () => {
    const store = ReactOnRails.getStore('GameResultStore')

    const reactComponent = (
        <Provider store={store}>
               <GameResultWidget />
        </Provider>
    )
    return reactComponent
}

export default mainNode
