import Constants from '../constants/GameResultConstants'
import {Helpers as TimelineHelpers} from '../../game-timeline/actions/GameTimelineActions';
import {EntryTypesConstants} from '../../game-timeline/containers/GameTimeline'

const Actions = {
	openGameResultDetailModal(show) {
		return dispatch => {
	        dispatch({ type: Constants.SHOW_GAME_RESULT_DETAIL_MODAL, show: show})
	    }	
	},
	changeTeamResult(teamType, value = 0) {
		return (dispatch, getState) => {

			let newResultState = _.clone(getState().gameResult);
			let teamDetailIndex = teamType === "homeTeamResult" ? "homeTeam" : "awayTeam"
			newResultState[teamType] =  parseInt(value);
	        dispatch({ type: Constants.CHANGE_TEAM_RESULT,  gameResult: newResultState});
		
	    }	
	},
	updateGoalsLines() {
		return (dispatch, getState) => {

		}
	},
	saveResult(saveUrl, info, showSuccessMsg =  true, callback) {
		
		 return (dispatch, getState) => {  
		 	let currentState =  getState();

		 	dispatch({ type: Constants.SAVING, state: true})
			ajaxCall({
	            ajax: {
	                url: saveUrl,
	                data: info,
	                success: function(data) {
	                    if(data.ok) {
	                        dispatch({ type: Constants.SAVING, state: false})

	                        if(showSuccessMsg) {
	        			    	$("#messages-container").html(getMessageLabel('success', data.msg))
	                        }

	                       	Helpers.updateGameResult(currentState, {awayTeamResult: info.awayTeamResult, homeTeamResult: info.homeTeamResult }, dispatch, callback);
	        				
	        				if(!data.result) return;

	        				let goalEntries = [...data.result.homeTeam, ...data.result.awayTeam] 
	        				let originalEntries = [...info.originalResultDetails.homeTeam, ...info.originalResultDetails.awayTeam];
	        				Helpers.updateEntries(currentState,  goalEntries, dispatch, originalEntries);

	        				if(typeof callback === 'function') {
	        					callback({homeTeam: Helpers.cleanEntries(data.result.homeTeam), awayTeam: Helpers.cleanEntries(data.result.awayTeam) });
	        				}

	                    } else{
	                        dispatch({ type: Constants.SAVING, state: false})
	        			    $("#messages-container").html(getMessageLabel('error', data.msg))
	                    }
	                }
	            }
	        })
		 }
	}
	
}
const Helpers = {

	updateGameResult(currentState, newResult, dispatch, callback) {
		let newGameResultInfo = {...currentState.gameResult, ...newResult}
		if(typeof callback === 'function') {
			callback();
		}
		dispatch({ type: Constants.CHANGE_TEAM_RESULT,  gameResult: newGameResultInfo})
	},
	cleanEntries(entries) {
		return entries.map((e) => {
			let mappedEntry = _.mapValues(e, function(o) { 
	            return _.isObject(o) ? o.id : o;
	        })
	        return  mappedEntry;
		})
	},
	updateEntries(currentState, entriesToUpdate, dispatch, originalEntries) {
		let gameEntries = [...currentState.gameResult.gameReportEntries];

		entriesToUpdate = [...Helpers.cleanEntries(entriesToUpdate)]
		
		entriesToUpdate.forEach((e) => {
			gameEntries = TimelineHelpers.updateItem(gameEntries, e.id, e, true, true);
		})

		let deletedElements = _.differenceBy( originalEntries.map((e) => { return {id: e}}) , entriesToUpdate , 'id').map((e) => { return parseInt(e.id) });
		gameEntries = gameEntries.filter((e) => { return deletedElements.indexOf(parseInt(e.id)) == -1  })
		
	    dispatch({ type: Constants.UPDATE_GAME_TIMELINE_ENTRIES, gameReportEntries: gameEntries })
	}
}
export default Actions