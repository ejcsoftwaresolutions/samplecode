import Constants from '../constants/GamePlayerReviewConstants'
import MetricConstants from 'base-coachy-components/GeneralComponents/template2/metric-responses/constants/MetricTypeConstants';


const Actions = {
    updatePlayerReview: (playerId, reviewChange, gameReviews) => {

        let review = _.find(gameReviews, (o) => {
            return o.player ? (o.player == playerId) : false;
        });
        if (!review) {
            review = {
                value: {
                    score: 0,
                    comment: '',
                },
                player: playerId,
                constant: MetricConstants.METRIC_REVIEW
            }
            review = { ...review,
                value: { ...review.value,
                    ...reviewChange
                }
            };
            gameReviews.push(review);
        } else {
            review.value = { ...review.value,
                ...reviewChange
            }
        }

        return dispatch => {
            dispatch({
                type: Constants.UPDATE_PLAYER_REVIEWS,
                gameReviews: gameReviews
            })
        }
    },
    hideAlertText() {

        return dispatch => {
            dispatch({
                type: Constants.HIDE_ALERT_TEXT
            })
        }
    },
    savePlayerReviews(saveUrl, playerReviews, gameId) {

        return dispatch => {
            dispatch({
                type: Constants.SAVING
            })

            ajaxCall({
                ajax: {
                    btn: null,
                    url: saveUrl,
                    data: {
                        playerReviews: playerReviews,
                        gameId: gameId
                    },
                    success: function(data) {
                        dispatch({
                            type: Constants.SAVED,
                            alertText: data.msg
                        })

                        playerReviews = playerReviews.map((e) => {
                            let newReview = _.find(data.results, (o) => {
                                return o.base_resource_id == e.player
                            })
                            return { ...e,
                                id: newReview.id,
                                value: { ...e.value,
                                    id: newReview.value.id
                                }
                            }
                        })
                        dispatch({
                            type: Constants.UPDATE_SAVED_REVIEWS,
                            gameReviews: playerReviews
                        })
                    }
                }
            })
        }

    }


}
export default Actions