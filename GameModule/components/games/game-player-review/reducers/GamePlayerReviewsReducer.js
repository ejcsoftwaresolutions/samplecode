import Constants from '../constants/GamePlayerReviewConstants'

export const initialState = {
	playerSelection: [],
	gameReviews: [],
	saving: false,
    alertText: ''
}

export default function GamePlayerReviewsReducer(state = initialState, action) {
    switch (action.type) {
    	case Constants.UPDATE_PLAYER_REVIEWS:
    		return {...state, gameReviews: action.gameReviews}
    		break;
		case Constants.SAVING:
    		return {...state, saving: true}
    		break;
	    case Constants.SAVED:
    		return {...state,  saving: false, alertText: action.alertText}
        case Constants.UPDATE_SAVED_REVIEWS:
            return {...state,  gameReviews: action.gameReviews}
    		break;		
        case Constants.HIDE_ALERT_TEXT:
            return {...state,  alertText: ''}
            break;      
	    default:
	        return state
    }
}
