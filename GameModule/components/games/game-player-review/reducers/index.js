import GamePlayerReviewsReducer from './GamePlayerReviewsReducer'
import { initialState as gameReviews } from './GamePlayerReviewsReducer'
import { combineReducers }  from 'redux'

// Combine all reducers you may have here
export default combineReducers({
    gameReviews: GamePlayerReviewsReducer,
    
})

export const initialStates = {
    gameReviews,
}
