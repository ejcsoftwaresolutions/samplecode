import React from 'react'
import Actions from '../actions/GamePlayerReviewsActions'
import { connect } from 'react-redux'
import PlayerReviewsWrapperTemplate from '../templates/PlayerReviewsWrapperTemplate.rt'
import { Provider } from 'react-redux'

class GamePlayerReviewsWrapper extends React.Component {

	constructor(props, context) {
		super(props, context);	
	}

	updateReview(playerId, value) {
		const {dispatch} = this.props;
		dispatch(Actions.updatePlayerReview(playerId, value, this.props.gameReviews.gameReviews ));
	}

	hideWarningMessage() {
		const {dispatch} = this.props;
		dispatch(Actions.hideAlertText());	
	}

	onReviewChanged() {
		
	}

	save() {
		const {dispatch} = this.props;
		dispatch(Actions.savePlayerReviews(this.props.mainConfig.saveUrl, this.props.gameReviews.gameReviews, this.props.gameReviews.gameId ));
	}

	getReviewData(playerId) {
        let reviewData = _.find(this.props.gameReviews.gameReviews, (e) => { return e.player == playerId });
        if(!reviewData) {
            reviewData = {
                value: {
                    score: 0,
                    comment: ''
                }
            }
        }
        return reviewData
    }

    handleStarRating(player, rating) {
		this.update(null, "score", rating, player);
	}
	
	update(e, prop = null, value = null, player = null) {
		let valueObject = {};

		if ( e !== null ) {
			prop = e.currentTarget.getAttribute("name");
  			valueObject[prop] = e.currentTarget.value;
  			player = e.currentTarget.getAttribute("data-key");
		} else {

			if(!prop || !value) return;

			valueObject[prop] = value;
		}

		this.updateReview(player, valueObject);
	}

    render() {
        return PlayerReviewsWrapperTemplate.apply(this)
    }

}


const mapStateToProps = (state) => (
{
	"gameReviews" : state.gameReviews,
	"activePlayers" : state.gameReviews.activePlayers,
	"mainConfig" : state.gameReviews.mainConfig,
	"trans" : state.gameReviews.trans
})

GamePlayerReviewsWrapper = connect(mapStateToProps)(GamePlayerReviewsWrapper)


const mainNode = () => {
    const store = ReactOnRails.getStore('GamePlayerReviewsStore')

    const reactComponent = (
        <Provider store={store}>
               <GamePlayerReviewsWrapper />
        </Provider>
    )
    return reactComponent
}

export default mainNode
