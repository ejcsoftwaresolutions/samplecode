import React from 'react'
import GameProgressBarTemplate  from "../templates/GameProgressBar.rt";

class GameProgressBar extends React.Component {

	constructor(props, context) {
		super(props, context);	
	}

	componentDidMount() {
	}

    render() {  
    	return GameProgressBarTemplate.apply(this);
    }
}

GameProgressBar.defaultProps = {
	split: true,
	showLabels: true,
	showBar: 'both'
}

export default GameProgressBar;