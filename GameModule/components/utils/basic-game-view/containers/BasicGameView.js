import React from 'react'
import BasicGameViewTemplate  from "../templates/BasicGameView.rt";

class BasicGameView extends React.Component {

	constructor(props, context) {
		super(props, context);	
	}

	componentDidMount() {
	}

    render() {  
    	return BasicGameViewTemplate.apply(this);
    }
}

BasicGameView.defaultProps = {
	openQuickResultModal: true,
	showGameProgressBars: true,
}

export default BasicGameView;