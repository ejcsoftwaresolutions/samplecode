import Constants from '../constants/FormationBuilderConstants'
import { Helpers } from '../containers/FormationBuilder';

const Actions = {

	activateTool(tool, canvas) {
		canvas.selection = true;
		canvas.defaultCursor= "normal";
		return dispatch => {
            dispatch({ type: Constants.ACTIVATE_TOOL, activeTool: tool })
        }
	}
}

export default Actions
