import Constants from '../constants/FormationBuilderConstants'
import { Helpers } from '../containers/FormationBuilder';
import { getFieldAreaContainerBaseParameters } from '../containers/Field'

const Actions = {

	prepareFieldConfig(fieldConfig) {

		return dispatch => {
			dispatch({ type: Constants.PREPARE_FIELD_AREAS, fieldConfig: fieldConfig })
		}
	},
	insertPlayerPosition(positions, position, coordinates) {

		let positionType = positions.filter((e, i) =>  { 
			return parseInt(e.playerPosition) == parseInt(position.id) 
		});
		return dispatch => {
			dispatch({ type: Constants.INSERT_PPOSITION, position: {
				playerPosition: position.id,
				coordinates: coordinates,
				viewId: Functions.guid(),
				text: position.translation
			}, activePosition: position })
		}
	},
	updateSpotArea(spotId, area, positions, fieldConfig) {
		const position = positions.filter((e, i) => {  
			return e.viewId == spotId 
		})[0];
		fieldConfig.areas[position.area].playerAmount--;
		fieldConfig.areas[area].playerAmount++;

		position.area = area;

		return dispatch => {
			dispatch({ type: Constants.MOVE_POSITION_AREA, fieldConfig: fieldConfig, position: positions })
		}
	},
	updateSpotCoordinates(positions, viewId, coordinates) {

		const position = positions.filter((e, i) => {  
			return e.viewId == viewId 
		})[0];

		position.coordinates = coordinates;

		return dispatch => {
			dispatch({ type: Constants.UPDATE_POSITIONS, positions: positions })
		}

	},
	changeLineupName(name) {
		return dispatch => {
			dispatch({ type: Constants.CHANGE_NAME, name: name })
		}
	},
	saveLineup(url, data, thumbnail) {
		data =  _.clone(data);
		delete data.canvas;
		delete data.template;

		let formData =  new FormData();
		formData.append('thumbnail', thumbnail);
		formData.append("data", JSON.stringify(data) )
		
		return  dispatch => {

			dispatch({ type: Constants.SAVING, saving: true, warning: {text: "", type: "SUCCESS" }  })
			ajaxCall({
				ajax: {
					url: url,
					type: 'POST',
					dataType: 'json',
					processData: false,
	  				contentType: false,
					data: formData,
					success: function(data) {
						if(data.ok){
							dispatch({ type: Constants.SAVING, saving: false, warning: { text: data.msg, type: "SUCCESS"  }  })
							window.location.href = data.editUrl
						} else {
							dispatch({ type: Constants.SAVING, saving: false, warning: { text: data.msg, type: "DANGER"  }  })
						}
					}

				}
			})
			
		}
	},
	loadSavedPositions(positions) {
		return dispatch => {
			dispatch({ type: Constants.LOAD_SAVED_POSITIONS, positions: positions})
		}
	},
	drawPlayersOnField(playersPerArea, fieldConfigurations ) {

		fieldConfigurations.mainConfig.areas.attacking["playerAmount"] = playersPerArea[2];
		fieldConfigurations.mainConfig.areas.midfield["playerAmount"] = playersPerArea[1];
		fieldConfigurations.mainConfig.areas.defensive["playerAmount"] = playersPerArea[0];

		const fieldAreasInfo = Object.keys(Helpers.getFieldAreasInfo());
		const playerViewMetadata = { height: 86, width: 7 };
		let positions = [];

		playersPerArea.forEach((area, i) => {
			let areaParameters = {... fieldConfigurations.mainConfig.areas[fieldAreasInfo[i]], areaInfo: fieldAreasInfo[i]  }
			let  playersInArea = this.getFieldAreaPlayersPositions(areaParameters, fieldConfigurations,  {
				... playerViewMetadata, 
				icon:  fieldConfigurations.positionIcons['default'] 
			});
			positions = positions.concat(playersInArea)
		});
		
		positions.push( this.getGoalKeeperPosition(fieldConfigurations,  {
			... playerViewMetadata, 
			icon:  fieldConfigurations.positionIcons['goalkeeper'] 
		}));
		
		return dispatch => {
			dispatch({ type: Constants.DRAW_PLAYER_IN_AREAS, positions: positions, fieldConfig: fieldConfigurations.mainConfig })
		
		}
	},
	getFieldAreaPlayersPositions(areaParameters, fieldConfigurations, playerViewMetadata) {

		const containerHeight = fieldConfigurations.containerElement.height();
		const containerWidth = fieldConfigurations.containerElement.width();
		
		let DOMelement = $('#' + areaParameters.element);
		let areaHeight = DOMelement.height();
		let yMidPoint = ( DOMelement.height()/ 2 ).toFixed(2);
		yMidPoint = yMidPoint - (playerViewMetadata.height/2);
        yMidPoint = ( 100 * parseFloat(yMidPoint / parseFloat(areaHeight)) ) ;

		let areaOffset = {
			"defensive": 90,
			"midfield": 120,
			"attacking": 140
		}
		let spotWidth  = (containerWidth - areaOffset[areaParameters.name]*2) / areaParameters.playerAmount;
		var lastSidePosition = areaOffset[areaParameters.name] , xFrom, xTo, xMidPoint,  positions = [];


		for (var i = 0; i < areaParameters.playerAmount; i++) {

			xFrom = parseFloat(lastSidePosition);
			xTo = parseFloat( parseFloat(lastSidePosition) + parseFloat(spotWidth));
			xMidPoint = ( (xTo + xFrom) / 2 ).toFixed(2);
			
			xMidPoint = ( 100 * parseFloat(xMidPoint / parseFloat($(DOMelement).width())) ) ;
			xMidPoint = xMidPoint - (playerViewMetadata.width/2);

			positions.push({
				playerPosition: 0,
				coordinates: {
					x: xMidPoint,
					y: yMidPoint		
				},
				viewId: Functions.guid(),
				image: playerViewMetadata.icon,
				area:  areaParameters.areaInfo
			})

			lastSidePosition += spotWidth;
		}
		return positions
	},
	getGoalKeeperPosition(fieldConfigurations, playerViewMetadata) {
		let fieldAreaContainerBaseParameters = getFieldAreaContainerBaseParameters();
		const containerHeight = fieldConfigurations.containerElement.height();
		const containerWidth = fieldConfigurations.containerElement.width();

		let x = containerWidth/2 ;
		x = ( 100 * parseFloat(x / parseFloat(containerWidth)) ) ;
		x =  x -  (playerViewMetadata.width/2)
		return {
			playerPosition: 0,
			coordinates: {
				x: x,
				y: 0 //fieldAreaContainerBaseParameters.golkeeperAreaHeight/2 - (playerViewMetadata.height/2)	
			},
			viewId: Functions.guid(),
			image: playerViewMetadata.icon,
			area: "goalkeeper"
		};
	},
	showWarning(message, type = null, link = null, blockSaving = false) {

		let warning = message ?  {
			text: message,
			type: type,
			link: link
		} : null;
		return dispatch => {
			dispatch({ type: Constants.SHOW_ALERT, warning: warning, valid: !blockSaving })
		}
	},
	resizeFieldAreas(newFieldConfig) {

		return dispatch => {
			dispatch({ type: Constants.PREPARE_FIELD_AREAS, fieldConfig: newFieldConfig })
		}
	}
}

const Functions = {
	guid() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	    s4() + '-' + s4() + s4() + s4();
	}
}



export default Actions