import React from 'react'
import Actions from '../actions/FormationBuilderActions'
import MenuActions from '../actions/MenuActions'
import { connect } from 'react-redux'
import MainButtonsTemplate from  '../templates/MainButtonsTemplate.rt'
import domtoimage from 'dom-to-image';
import { Provider } from 'react-redux'

class MainButtons extends React.Component { 
	
	constructor(props, context) {
		super(props, context);	
	}

	saveLineup(e) {
		const {dispatch} = this.props;
		$(e.currentTarget).attr("disabled", "disabled")
		this.generateFieldThumbnailImage((thumbnail) => {
			dispatch(Actions.saveLineup(this.props.mainConfig.saveUrl, this.props.formationState, thumbnail));	
		});
	}

	generateFieldThumbnailImage(callback) {
		$(".field-area").addClass("hide");
		$(".position-area-name").hide();
		let targetImageElement = $("#fieldThumbnail")[0];
		domtoimage.toBlob($("#canvasWrapper")[0])
	    .then(function (blob) {
	    //    targetImageElement.src = dataUrl;
			$(".field-area").removeClass("hide");
			$(".position-area-name").show();
	        callback(blob)
	    })
	    .catch(function (error) {
	        console.error('oops, something went wrong when generating thumbnail: ' );
	    });
	}

	render() {
		return (
			MainButtonsTemplate.apply(this)
		)
	}

}

const mapStateToProps = (state) => (
{
	"formationState" : state.formationState,
	"mainConfig": state.formationState.mainConfig,
})

MainButtons = connect(mapStateToProps)(MainButtons)

const mainNode = () => {
    const store = ReactOnRails.getStore('FormationBuilderStore')

    const reactComponent = (
        <Provider store={store}>
               <MainButtons />
        </Provider>
    )
    return reactComponent
}

export default mainNode
