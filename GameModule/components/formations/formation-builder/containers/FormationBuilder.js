import React from 'react'
import Actions from '../actions/FormationBuilderActions'
import { connect } from 'react-redux'
import FormationBuilderTemplate from '../templates/FormationBuilderTemplate.rt'
import {fabric} from 'fabric';
import { Provider } from 'react-redux'

class FormationBuilder extends React.Component {

	constructor(props, context) {
		super(props, context);	
		
	}

	hideMessages() {
		const {dispatch} = this.props;
		dispatch(Actions.showWarning(null, null, null, !this.props.formationState.valid));	
	}

	componentDidMount() {
		
		const {dispatch} = this.props;

		if(!this.props.mainConfig.limits.maxNumPlayersInGame) {
			
			dispatch(Actions.showWarning(Translator.trans("max.players.in.field.no.config", {}, "formation"), "DANGER", this.props.mainConfig.routes.appSettings , true));
		}

		if(!this.props.mainConfig.limits.minNumPlayersInGame) {
			
			dispatch(Actions.showWarning(Translator.trans("min.players.in.field.no.config", {}, "formation"), "DANGER", this.props.mainConfig.routes.appSettings , true));
		}
		
		if(this.props.formationState.template) {
			let positions =  this.props.formationState.template.player_positions.map((e) => {
				return {
					coordinates: {
						x: e.x,
						y: e.y
					},
					viewId: e.view_id,
					image: e.area === "goalkeeper" ? this.props.mainConfig.positionsIcons.goalkeeper : this.props.mainConfig.positionsIcons.default,
					area: e.area
				}
			})
			
		    dispatch(Actions.loadSavedPositions(positions) );
		}

	}

	render() {
		return (
			FormationBuilderTemplate.apply(this)
		)
	}

}

export const Helpers = {

	getPlayerItem(config) {
		
		const groupCustomProps = {
			toolName: config.text,
			positionId: config.positionId,
			order: config.order
		}

		let g = new fabric.GenericPlayerView({
			props: config,
			...groupCustomProps,
			left: config.left,
			top: config.top
		});
		
		return g;
	},
	resizeCanvas(canvas, parent = window) {

		function objInArray(obj, annotationArray){
			for (var i = 0; i < annotationArray.length; i++) {
				if (annotationArray[i].obj === obj){
					return i;
				}
			} 
			return -1;
		}

		var annotationList = [];

		function Annotation(obj, x, y, width, height, vpW, vpH) {
			this.obj = obj;
			this.xCoord = x;
			this.yCoord = y;
			this.widthVal = width;
			this.heightVal = height;
			this.viewportWidth = vpW;
			this.viewportHeight = vpH;
		}  
		
		var vpW = parent.offsetWidth;
		var vpH = parent.offsetHeight;

		canvas.setHeight(vpH);
		canvas.setWidth(vpW);
		canvas.width = vpW;
		canvas.height = vpH;

		canvas.forEachObject(function(obj){
			if (objInArray(obj, annotationList) <0){
				var x = obj.get('left'),
				y = obj.get('top'),
				width = obj.getWidth(),
				height = obj.getHeight();
				var newAnnotation = new Annotation(obj, x, y, width, height, vpW, vpH);
				annotationList.push(newAnnotation);
			}

			var annotation = annotationList[objInArray(obj, annotationList)];

			var scaleW = (vpW/annotation.viewportWidth);
			var scaleH = (vpH/annotation.viewportHeight);
			var scaleX = annotation.xCoord * scaleW;
			var scaleY = annotation.yCoord * scaleW; 

			obj.set('scaleX', scaleW);
			obj.set('scaleY', scaleW); // preserve aspect ratio. Use scaleH to scale independently 
			obj.set('left', scaleX);
			obj.set('top', scaleY); 
			obj.setCoords();
		}); 

		canvas.renderAll();
	},
	createCanvas() {
		var canvas = document.createElement('canvas');
		var canvas = new fabric.Canvas(canvas, {
			isDrawingMode: false,
		});
		Helpers.resizeCanvas(canvas);
		return canvas;
	},

	getFieldAreasInfo() {
		return   {
			"attacking" : {
				"playerTypeName" : Translator.trans("coachy.forward", {}, "general"),
				"areaName": Translator.trans("coachy.attacking.area", {}, "general")
			},
			"midfield" : {
				"playerTypeName" : Translator.trans("coachy.midfielder", {}, "general"),
				"areaName": Translator.trans("coachy.midfield.area", {}, "general")
			},
			"defensive" : {
				"playerTypeName" : Translator.trans("coachy.defender", {}, "general"),
				"areaName": Translator.trans("coachy.defensive.area", {}, "general")
			},
			"goalkeeper" : {
				"playerTypeName" : Translator.trans("player.position.goalkeeper", {}, "systemVariables"),
				"areaName": Translator.trans("player.position.goalkeeper", {}, "systemVariables")
			},

		}
	}

}


const mapStateToProps = (state) => (
{
	"formationState" : state.formationState,
	"mainConfig": state.formationState.mainConfig,
})

FormationBuilder = connect(mapStateToProps)(FormationBuilder)

const mainNode = () => {
    const store = ReactOnRails.getStore('FormationBuilderStore')

    const reactComponent = (
        <Provider store={store}>
               <FormationBuilder />
        </Provider>
    )
    return reactComponent
}

export default mainNode
