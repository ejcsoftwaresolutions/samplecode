import React from 'react'
import {findDOMNode} from 'react-dom'
import Actions from '../actions/FormationBuilderActions'
import { connect } from 'react-redux'
import Field from './Field'
import {Helpers as CanvasHelpers} from  '../../utils/canvas-helpers/CanvasHelper'


class BaseTargetField extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return <Field props={{positions: this.props.formationState.positions}} /> 
	}

}

const mapStateToProps = (state) => (
{
  "formationState" : state.formationState,
  "mainConfig": state.formationState.mainConfig,
})


export default connect(mapStateToProps)(BaseTargetField) ;