import React from 'react'
import { connect } from 'react-redux'
import PositionSpotTemplate from '../templates/PositionSpotTemplate.rt'
import Actions from '../actions/FormationBuilderActions'

import {findDOMNode} from 'react-dom'

export class PositionSpot extends React.Component {

	constructor(props) {
		super(props);
	}

	componentDidMount() {

		if(!this.props.onlyView && this.props.position.area !== "goalkeeper") {
		  	$(findDOMNode(this)).draggable({
	  		 	"containment": $(".canvas-container")[0],
	 			"revert": "invalid",
		  	});
		}
	}	


	render() {
		return PositionSpotTemplate.apply(this);
	}
}

const mapStateToProps = (state) => (
    {
    "formationState" : state.formationState,
    "mainConfig": state.formationState.mainConfig,
    }
)

export default connect(mapStateToProps)(PositionSpot) ;

