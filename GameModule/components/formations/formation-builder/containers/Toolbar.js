import React from 'react'
import Actions from '../actions/FormationBuilderActions'
import { connect } from 'react-redux'
import ToolbarTemplate from  '../templates/ToolbarTemplate.rt'

class Toolbar extends React.Component { 
	constructor(props, context) {
		super(props, context);	
		this.state = {
			gamesAssociatedMsg: null,
			canAddOrDeletePositions: this.props.formationState.template ? this.props.formationState.template.can_add_or_delete_positions : true
		}
	}

	changeLineupName(e) {
		const name =  e.currentTarget.value;
		const {dispatch} = this.props;
		dispatch(Actions.changeLineupName(name));
	}

	saveLineup(e) {
		const {dispatch} = this.props;
		$(e.currentTarget).attr("disabled", "disabled")
		dispatch(Actions.saveLineup(this.props.mainConfig.saveUrl, this.props.formationState));	
	}

	hideMessages() {
		const {dispatch} = this.props;
		dispatch(Actions.showWarning(null, null, null, !this.props.formationState.valid));	
	}

	componentDidMount() {
		if(!this.state.canAddOrDeletePositions) {
			this.setState({
				gamesAssociatedMsg: {
					text: Translator.trans("formation.with.games.associated.warning", {}, "formation")
				}
			})
		}
	}

	onSubmitFormationParameters() {

		const {dispatch} = this.props;
		
		let formationParameters = ToolBarHelpers.getInputAreasParameters.bind(this)();

		if(!ToolBarHelpers.validateFieldAreasInputParameters.bind(this)(formationParameters)) {
			return false;
		}

		let fieldConfigurations = {
			mainConfig: {...this.props.formationState.fieldConfig},
			positionIcons: { 
			  default: this.props.mainConfig.positionsIcons.default, 
			  goalkeeper: this.props.mainConfig.positionsIcons.goalkeeper
			},
			containerElement: $("#canvasWrapper")
		}

		dispatch(Actions.drawPlayersOnField(formationParameters, fieldConfigurations));	
	}

	render() {
		return (
			ToolbarTemplate.apply(this)
		)
	}


}

const ToolBarHelpers = {

	getInputAreasParameters() {
		let areas = [];
		
		if($(".area-player-amount").length > 0) {
			areas = $(".area-player-amount").map((i,e ) => {
				//if($(e).val() == "") ;
				return $(e).val();
			}).toArray()
		}
		return areas;
	},

	validateFieldAreasInputParameters(formationParameters) {
		const {dispatch} = this.props;
		let valid = true;
		const maxNumPlayersInGame = this.props.mainConfig.limits.maxNumPlayersInGame;
		const minNumPlayersInGame = this.props.mainConfig.limits.minNumPlayersInGame;

		var totalAskedPlayers = formationParameters.reduce(function(sum, value) {
		  return sum + parseInt(value);
		}, 0);
			
		if(totalAskedPlayers > maxNumPlayersInGame - 1) {
			dispatch(Actions.showWarning(Translator.trans("max.players.reached", {maxPlayers: maxNumPlayersInGame }, "formation"), "WARNING", null, false));
			return false;

		} else if(totalAskedPlayers  + 1 < minNumPlayersInGame) {
			dispatch(Actions.showWarning(Translator.trans("min.players.needed", {minPlayers: minNumPlayersInGame }, "formation"), "WARNING", null, false));
			return false;
		}

		return valid;
	},

}

const mapStateToProps = (state) => (
{
	"formationState" : state.formationState,
	"mainConfig": state.formationState.mainConfig,
})

export default connect(mapStateToProps)(Toolbar)