import React from 'react'

const BaseTool = (props) => (

	<li className='tool-group'>
		{ props.children }
	</li>
)

export default BaseTool;