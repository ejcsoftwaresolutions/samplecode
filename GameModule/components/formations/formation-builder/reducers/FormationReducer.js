import Constants from '../constants/FormationBuilderConstants'
import { Helpers } from '../containers/FormationBuilder';
import Actions from '../actions/FormationBuilderActions'

export const initialState = {
 	name: '',
    id: null,
    template: null,
    editUrl: null,
    defaultView: null,
 	positions: [],
    activeTool: null,
    activePosition: null,
 	mainConfig: {},
    saving: false,
    warning: null,
    lines: [],
    valid: true,
    thumbnail: null,
}

export default function FormationBuilderReducer(state = initialState, action) {
    switch (action.type) {
    	case  Constants.INSERT_PPOSITION:
    		return {...state, positions: [...state.positions, action.position], activePosition: action.activePosition }
    		break;
    	case  Constants.CHANGE_NAME:
    		return {...state, name: action.name}
    		break;
        case Constants.LOAD_FROM_TEMPLATE:
            return {...state, positions: [...action.positions]}
            break;
        case  Constants.ACTIVATE_TOOL:
            return {...state, activeTool: action.activeTool, activePosition: null  }
            break;
        case  Constants.SAVING:
            return {...state, saving: action.saving, warning: action.warning }
            break;
        case  Constants.UPDATE_POSITIONS:
            return {...state, positions: [...action.positions] }
            break;
        case Constants.LOAD_SAVED_POSITIONS:
            return {...state, positions: [...action.positions]}
            break;
        case Constants.DRAW_PLAYER_LINES:
            return {...state, lines: action.lines, positions: [...action.positions] }
            break;
        case Constants.SHOW_ALERT:
            return {...state, warning: action.warning, valid: action.valid }
            break;
        case Constants.PREPARE_FIELD_AREAS:
            return {...state, fieldConfig: action.fieldConfig }
            break;
        case Constants.DRAW_PLAYER_IN_AREAS:
            return {...state, fieldConfig: action.fieldConfig, positions: [...action.positions] }
            break;
        case Constants.MOVE_POSITION_AREA:
            return {...state, fieldConfig: action.fieldConfig, positions: action.positions }
            break;
	    default:
	        return state
    }
}
