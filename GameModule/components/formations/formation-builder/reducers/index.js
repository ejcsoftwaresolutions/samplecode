import FormationReducer from './FormationReducer'

import { initialState as formationState } from './FormationReducer'

import { combineReducers }  from 'redux'

// Combine all reducers you may have here
export default combineReducers({
    formationState: FormationReducer,
    
})

export const initialStates = {
    formationState,
}
