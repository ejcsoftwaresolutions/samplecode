import { combineReducers }  from 'redux'
import GameOverviewReducer from '../../components/games/overview/reducers/'
import { initialStates as GameOverviewState } from '../../components/games/overview/reducers/'


// Combine all reducers you may have here
export default combineReducers({
    gameOverview: GameOverviewReducer,
})

export const initialStates = {
	gameOverview: GameOverviewState
}
