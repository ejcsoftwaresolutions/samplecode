import { combineReducers }  from 'redux'
import FormationOverviewReducer from '../../components/formations/overview/reducers/'
import { initialStates as FormationOverviewState } from '../../components/formations/overview/reducers/'


// Combine all reducers you may have here
export default combineReducers({
    formationOverview: FormationOverviewReducer,
})

export const initialStates = {
	formationOverview: FormationOverviewState
}
