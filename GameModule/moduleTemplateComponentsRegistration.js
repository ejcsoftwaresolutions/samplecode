import ReactOnRails from 'react-on-rails'

/* Component registration of Game Module (only root components)*/
import GameShowPage  from './pages/games/show/containers/ShowPage';
import AddGameLineupPage  from './pages/games/add-game-lineup/containers/AddGameLineupPage';
import EditGameLineupPage  from './pages/games/edit-game-lineup/containers/EditGameLineupPage';


import FormationShowPage  from './pages/formations/show/containers/ShowPage';
import FormationAddPage  from './pages/formations/add/containers/AddPage';
import FormationEditPage  from './pages/formations/edit/containers/EditPage';

/* to use in edit pages for now, these should not be root components...*/
import GameTimelineComponent from './components/games/game-timeline/containers/GameTimeline'
import GamePlayerReviews from './components/games/game-player-review/containers/GamePlayerReviewsWrapper'
import GameLineupViewerComponent from './components/games/lineup-viewer/containers/LineupViewer'
import GameResultWidget from './components/games/game-result/containers/GameResultWidget'

import FormationBuilderComponent from './components/formations/formation-builder/containers/FormationBuilder';
import FormationBuilderMainButtons from './components/formations/formation-builder/containers/MainButtons';


ReactOnRails.register({ GameShowPage })
ReactOnRails.register({ AddGameLineupPage })
ReactOnRails.register({ EditGameLineupPage })
ReactOnRails.register({ GameResultWidget })
ReactOnRails.register({ GamePlayerReviews })



ReactOnRails.register({ GamesOnboarding })
ReactOnRails.register({ FormationsOnboarding })



ReactOnRails.register({ FormationShowPage })
ReactOnRails.register({ FormationAddPage })
ReactOnRails.register({ FormationEditPage })
ReactOnRails.register({ FormationBuilderComponent })
ReactOnRails.register({ FormationBuilderComponent })



ReactOnRails.register({ GameLineupViewerComponent })
ReactOnRails.register({ FormationBuilderMainButtons })

