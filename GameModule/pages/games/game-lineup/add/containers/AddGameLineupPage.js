import React from 'react'
import AddGameLineupPageTemplate from '../templates/AddGameLineupPage.rt'

class AddGameLineupPage extends React.Component {
    
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
       return AddGameLineupPageTemplate.apply(this)
    }   
}

AddGameLineupPage.defaultProps = {

}

export default AddGameLineupPage
