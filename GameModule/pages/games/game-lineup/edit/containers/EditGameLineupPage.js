import React from 'react'
import EdiGameLineupPageTemplate from '../templates/EdiGameLineupPage.rt'

class EdiGameLineupPage extends React.Component {
    
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
       return EdiGameLineupPageTemplate.apply(this)
    }   
}

EdiGameLineupPage.defaultProps = {

}

export default EdiGameLineupPage
