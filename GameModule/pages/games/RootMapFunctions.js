import { OverviewModules } from 'base-coachy-components/GeneralComponents/template2/overview/shared/OverviewModules'
import { Helpers } from 'base-coachy-components/GeneralComponents/template2/overview/shared/OverviewGeneralActions'

export const MapFunctions = {

    OverviewPage : (initialStates, props) => {
        let GeneralOverviewState = initialStates.modules.game.gameOverview.GeneralOverviewState;
        let LocalOverviewState = initialStates.modules.game.gameOverview.LocalOverviewState;

        let items = props.items;

        /* Deleting items from props*/
        delete props['items']

        let savedOverviewConfig = _.clone(props.overviewConfig);
        let listViewDisplay = null;

        if(!_.isNull(savedOverviewConfig) && !_.isUndefined(savedOverviewConfig["listViewDisplay"])) {
            listViewDisplay = savedOverviewConfig["listViewDisplay"];
            delete savedOverviewConfig["listViewDisplay"];
        }

        let overviewConfig = !_.isUndefined(savedOverviewConfig) &&  !_.isNull(savedOverviewConfig)  ? 
                            {
                                ...GeneralOverviewState.moduleOverviewConfig, 
                                ... savedOverviewConfig 
                            }
                            : 
                            {
                                ...GeneralOverviewState.moduleOverviewConfig, 
                                "activeView": 'list',
                                sorting: {
                                    'by': "datetime",
                                    'type': 'asc'
                                }
                            };

        let orderedItems = Helpers.sort(overviewConfig.sorting.type, overviewConfig.sorting.by, items);

        let filteredItems = null;

        if(!_.isUndefined(overviewConfig.filtersConfig) && !_.isEmpty(overviewConfig.filtersConfig)) {
            filteredItems = Helpers.filterItems(orderedItems, overviewConfig.filtersConfig);
        }

        return  { 
            ...initialStates,
            modules: {
                ...initialStates.modules,
                game: {
                    gameOverview: {
                        LocalOverviewState: { ...LocalOverviewState, ...props, listViewDisplay: !_.isNull(listViewDisplay) ? listViewDisplay : 'upcoming'  },
                        GeneralOverviewState:{...GeneralOverviewState, module: OverviewModules.Games , moduleOverviewConfig: {...overviewConfig }, items: orderedItems, searchResults: filteredItems, searching: !_.isNull(filteredItems), filtersConfig: !_.isUndefined(overviewConfig.filtersConfig) && !_.isEmpty(overviewConfig.filtersConfig) ? overviewConfig.filtersConfig : null },
                    }
                }
            }
        }
    }
}