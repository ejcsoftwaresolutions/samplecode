<rt-import name="default" as="PropertyDisplay" from="base-coachy-components/GeneralComponents/template2/property-display/containers/PropertyDisplay"/>
<rt-import name="Button" as="Button" from="mdbreact"/>
<rt-import name="default" as="WidgetCard" from="base-coachy-components/GeneralComponents/template2/widget-card/containers/WidgetCard"/>
<rt-import name="Progress" from="mdbreact"/>
<rt-import name="Tooltip" from="mdbreact"/>

<div class="row" rt-scope="this.props.game as game;  this.props.routes as routes">
	<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5 mb-3">

		<WidgetCard title="{Translator.trans('game.preparation.widget.title', {}, 'game')}" icon="{this.props.routes.assetsPath.replace('PATH', 'img/illustrations/game/games.svg')} ">
			<Tooltip tag="div" tooltipContent="{Translator.trans('coachy.completeness', {}, 'general')}: {game.completeness}%" placement="top">
				<Progress value="{game.completeness}" color="success" class="thick-progress"></Progress>
			</Tooltip>

			<div class="list-group checklist">
				<a href="{_.isNull(game.opponent_team) ?  routes.editLink : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': !_.isNull(game.opponent_team)}" >
					<span>
						<input disabled checked="{!_.isNull(game.opponent_team)}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('game.preparation.widget.opponent.item', {}, "game")}
		    	</a>
		    	<a href="{!game.has_basic_info_completed ?  routes.editLink : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': game.has_basic_info_completed}">
					<span>
						<input disabled checked="{game.has_basic_info_completed}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('game.preparation.widget.basic.info.item', {}, "game")}
		    	</a>
		    	<a href="{!game.has_selection ?  routes.editLink + '?section=selection' : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': game.has_selection}">
					<span>
						<input disabled checked="{game.has_selection}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('game.preparation.widget.game.selection.item', {}, "game")}
		    	</a>
		    	<a href="{!game.has_lineup ?  routes.editLink + '?section=lineup' : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': game.has_lineup}">
					<span>
						<input disabled checked="{game.has_lineup}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('game.preparation.widget.game.lineup.item', {}, "game")}
		    	</a>
			</div>
		</WidgetCard>

		<WidgetCard title="{Translator.trans('post.game.tasks.widget.title', {}, 'game')}" icon="{this.props.routes.assetsPath.replace('PATH', 'img/illustrations/game/games.svg')} " class="mt-3">
			<Tooltip tag="div" tooltipContent="{Translator.trans('coachy.completeness', {}, 'general')}: {game.after_game_completeness}%" placement="top">
				<Progress value="{game.after_game_completeness}" color="success" class="thick-progress"></Progress>
			</Tooltip>

			<div class="list-group checklist">
				<a href="{!game.has_result ?  routes.editLink : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': game.has_result}" >
					<span>
						<input disabled checked="{game.has_result}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('post.game.tasks.widget.result.item', {}, "game")}
		    	</a>
		    	<a href="{!game.has_report ?  routes.editLink + '?section=report' : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': game.has_report}">
					<span>
						<input disabled checked="{game.has_report}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('post.game.tasks.widget.comments.item', {}, "game")}
		    	</a>
		    	<a href="{!game.has_timeline ?  routes.editLink + '?section=timeline' : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': game.has_timeline}">
					<span>
						<input disabled checked="{game.has_timeline}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('post.game.tasks.widget.timeline.item', {}, "game")}
		    	</a>
		    	<a href="{!game.has_reviews ?  routes.editLink + '?section=reviews' : 'javascript:void(0);' }" class="list-group-item list-group-item-action" rt-class="{ 'list-item-done': game.has_reviews}">
					<span>
						<input disabled checked="{game.has_reviews}" type="checkbox" class="checkbox form-check-input" />
						<label> </label> 
					</span>
			    	{ Translator.trans('post.game.tasks.widget.reviews.item', {}, "game")}
		    	</a>
			</div>
		</WidgetCard>

	</div>

    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
		<WidgetCard title="{Translator.trans('game.fiche.section.title', {}, 'game')}" icon="{this.props.routes.assetsPath.replace('PATH', 'img/illustrations/game/games.svg')} " >
            <div class="table-responsive">
				<table class="table table-striped property-display-table" >
					<tr>
	                    <td class="font-weight-bold green-text">{ Translator.trans('coachy.title', {}, 'general') }</td>
	                    <td>
	                    	<PropertyDisplay 
	                    		property="{this.toHTML(game.name_html)}" 
	                    		editLink="{routes.editLink}"
	                    		addLink="{routes.editLink}" 
	                    		isEditable="{false}"
	                    	/>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="font-weight-bold green-text">{ Translator.trans('game.opponent.team', {}, 'game') }</td>
	                    <td>
	                    	<PropertyDisplay 
	                    		property="{game.opponent_team.name}" 
	                    		editLink="{routes.editLink}"
	                    		addLink="{routes.editLink}" 
	                    		showLink="{routes.showOpponentLink.replace('ID', game.opponent_team.id)}" 
	                    	/>
	                    </td>
	                </tr>

	              	<tr>
	                    <td class="font-weight-bold green-text">{ Translator.trans('coachy.date.word', {}, 'general') }</td>
	                    <td>
	                    	<PropertyDisplay 
	                    		property="{game.date ? moment(game.date).format(window.userSettings.dateFormat) : null}" 
	                    		editLink="{routes.editLink}"
	                    		addLink="{routes.editLink}" 
	                    	/>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="font-weight-bold green-text">{ Translator.trans('coachy.time.word', {}, 'general') }</td>
	                    <td>
	                    	<PropertyDisplay 
	                    		property="{game.time ? moment(game.time).format(window.userSettings.timeFormat) : null}" 
	                    		editLink="{routes.editLink}"
	                    		addLink="{routes.editLink}" 
	                    	/>
	                    </td>
	                </tr>
	             	<tr>
	                    <td class="font-weight-bold green-text">{ Translator.trans('coachy.league', {}, 'general') }</td>
	                    <td>
	                    	<PropertyDisplay 
	                    		property="{game.league ? game.league : null}" 
	                    		editLink="{routes.editLink}"
	                    		addLink="{routes.editLink}" 
	                    	/>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="font-weight-bold green-text">{ Translator.trans('coachy.location.type', {}, 'general') }</td>
	                    <td>
	                    	<PropertyDisplay 
	                    		property="{Translator.trans(game.location_type.translation_key, {}, 'systemVariables')}" 
	                    		editLink="{routes.editLink}"
	                    		addLink="{routes.editLink}" 
	                    	/>
	                    </td>
	                </tr>
	              	<tr>
	                    <td class="font-weight-bold green-text">{ Translator.trans('coachy.game.type', {}, 'general') }</td>
	                    <td>
	                    	<PropertyDisplay 
	                    		property="{Translator.trans(game.game_type.translation_key, {}, 'systemVariables')}" 
	                    		editLink="{routes.editLink}"
	                    		addLink="{routes.editLink}" 
	                    	/>
	                    </td>
	                </tr>
				</table>
			</div>
		</WidgetCard>

		<WidgetCard title="{Translator.trans('game.address.section.title', {}, 'game')}" icon="{this.props.routes.assetsPath.replace('PATH', 'img/icons/marker.png')} " class="mt-3" >
			<div style="width: 100%; height: 300px; margin-top: 10px;" class="map" id="map-detail"></div>
	           	<input type="hidden" class="address-autocomplete" value="{game.address}">
	           	
           	<div class="mt-2">
				<PropertyDisplay 
	            		property="{game.address}" 
	            		editLink="{routes.editLink}"
	            		addLink="{routes.editLink}"
	            	/>
           	</div>
		</WidgetCard>
		
	</div>

</div>