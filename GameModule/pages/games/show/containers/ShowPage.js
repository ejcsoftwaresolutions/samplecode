import React from 'react'
import ShowPageTemplate from '../templates/ShowPage.rt'

class ShowPage extends React.Component {
    
    constructor(props) {
        super(props);
      	this.state = {
            activeTab: props.data.activeSection
        }
        
        this.changeActiveTab = this.changeActiveTab.bind(this);
    }

    toHTML(content) {
        return (<div dangerouslySetInnerHTML={{__html: content }} />)
    }
    
  	changeActiveTab(e) {
        let tabIndex = $(e.currentTarget).attr('tab');
        
        this.setState({
            activeTab: tabIndex
        })
    }

    componentDidMount() {

    }

    render() {
       return ShowPageTemplate.apply(this)
    }   
}

ShowPage.defaultProps = {

}

export default ShowPage
