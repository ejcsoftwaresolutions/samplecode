import React from 'react';
import GameDetailsTemplate from'../../templates/sections/GameDetails.rt'

class GameDetails extends React.Component {

	constructor(props) {
		super(props);
	}

 	toHTML(content) {
        return (<div dangerouslySetInnerHTML={{__html: content }} />)
    }
    
 	componentDidMount() {
        this.initMap()
    }

    initMap() {
        
        if($('.address-autocomplete').val()) {
            let _latitude = 0;
            let _longitude = 0;
            let element = "map-detail";
            let searchInput = $('.address-autocomplete');

            let markerImgUrl = this.props.routes.assetsPath.replace('PATH', 'img/marker.png');
            let place = searchInput.val() ? searchInput.val().trim() : '';
            place = place !== ''
                  ? place
                  : null;
            MapFunctions.simpleMap(_latitude, _longitude, element, false, place, markerImgUrl);
        }
   
    }

	render() {
		return GameDetailsTemplate.apply(this);
	}
}
export default GameDetails;
