import React from 'react';
import GameLineupTemplate from'../../templates/sections/GameLineup.rt'

class GameLineup extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return GameLineupTemplate.apply(this);
	}
}
export default GameLineup;
