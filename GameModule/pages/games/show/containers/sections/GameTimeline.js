import React from 'react';
import GameTimelineTemplate from'../../templates/sections/GameTimeline.rt'

class GameTimeline extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return GameTimelineTemplate.apply(this);
	}
}
export default GameTimeline;
