import React from 'react';
import GameReportTemplate from'../../templates/sections/GameReport.rt'

class GameReport extends React.Component {

	constructor(props) {
		super(props);
	}

 	toHTML(content) {
        return (<div dangerouslySetInnerHTML={{__html: content }} />)
    }
    

	render() {
		return GameReportTemplate.apply(this);
	}
}
export default GameReport;
