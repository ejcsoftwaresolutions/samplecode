import React from 'react';
import GameSelectionTemplate from'../../templates/sections/GameSelection.rt'

class GameSelection extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return GameSelectionTemplate.apply(this);
	}
}
export default GameSelection;
