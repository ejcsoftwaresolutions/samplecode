import React from 'react';
import GameReviewsTemplate from'../../templates/sections/GameReviews.rt'

class GameReviews extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return GameReviewsTemplate.apply(this);
	}
}
export default GameReviews;
