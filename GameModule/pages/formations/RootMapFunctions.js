import { OverviewModules } from 'base-coachy-components/GeneralComponents/template2/overview/shared/OverviewModules'
import { Helpers } from 'base-coachy-components/GeneralComponents/template2/overview/shared/OverviewGeneralActions'

export const MapFunctions = {

    OverviewPage : (initialStates, props) => {

        let GeneralOverviewState = initialStates.modules.formation.formationOverview.GeneralOverviewState;
        let LocalOverviewState = initialStates.modules.formation.formationOverview.LocalOverviewState;

        let items = props.items;

        /* Deleting items from props*/
        delete props['items']
        let overviewConfig = !_.isUndefined(props.overviewConfig) &&  !_.isNull(props.overviewConfig)  ? 
                                {
                                    ...GeneralOverviewState.moduleOverviewConfig, 
                                    ... props.overviewConfig 
                                }
                                : 
                                {
                                    ...GeneralOverviewState.moduleOverviewConfig, 
                                    "activeView": 'list',
                                    sorting: {
                                        'by': "name",
                                        'type': 'asc'
                                    }
                                };
        let orderedItems = Helpers.sort(overviewConfig.sorting.type, overviewConfig.sorting.by, items);

        return  { 
            ...initialStates,
            modules: {
                formation: {
                    formationOverview: {
                        LocalOverviewState: { ...LocalOverviewState, ...props},
                        GeneralOverviewState:{...GeneralOverviewState, module: OverviewModules.Formations , moduleOverviewConfig: {...overviewConfig }, items: orderedItems},
                    }
                }
            }
        }
    }
}