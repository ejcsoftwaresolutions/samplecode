import React from 'react'
import EditPageTemplate from '../templates/EditPage.rt'

class EditPage extends React.Component {
    
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
       return EditPageTemplate.apply(this)
    }   
}

EditPage.defaultProps = {

}

export default EditPage
