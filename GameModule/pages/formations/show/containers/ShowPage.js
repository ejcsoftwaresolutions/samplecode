import React from 'react'
import ShowPageTemplate from '../templates/ShowPage.rt'

class ShowPage extends React.Component {
    
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
       return ShowPageTemplate.apply(this)
    }   
}

ShowPage.defaultProps = {

}

export default ShowPage
