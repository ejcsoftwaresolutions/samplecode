import React from 'react'
import AddPageTemplate from '../templates/AddPage.rt'

class AddPage extends React.Component {
    
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
       return AddPageTemplate.apply(this)
    }   
}

AddPage.defaultProps = {

}

export default AddPage
